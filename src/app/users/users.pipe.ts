import {Pipe, PipeTransform} from '@angular/core';

import {User} from '../Models/User';

@Pipe({
    name: 'usersFilter'
})
export class UsersPipeFilter implements PipeTransform {
    transform(value: [User], filterType: string): [User] {
        if ( !value ) return value;
        const originalArr = JSON.parse(JSON.stringify(value));

        switch (filterType) {
            case 'order':
                return originalArr;
            case 'order-reverse':
                return originalArr.reverse();
            case 'firstName':
            case 'lastName':
                return originalArr.sort( (a, b) => {
                    const nameA = a[filterType].toLowerCase(),
                          nameB = b[filterType].toLowerCase();

                    if (nameA < nameB)
                        return -1;

                    if (nameA > nameB)
                        return 1;

                    return 0;
                });
            case 'firstName-reverse':
            case 'lastName-reverse':
                const type = filterType.split('-')[0];
                return originalArr.sort( (a, b) => {
                    const nameA = a[type].toLowerCase(),
                          nameB = b[type].toLowerCase();

                    if (nameA > nameB)
                        return -1;

                    if (nameA < nameB)
                        return 1;

                    return 0;
                });
            default:
                return originalArr;
        }
    }
}

@Pipe({
    name: 'usersSearch'
})
export class UsersPipeSearch implements PipeTransform {
    transform(value: [User], searchText: string): [User] {
        if ( !value || !searchText ) return value;

        const originalArr = JSON.parse(JSON.stringify(value))
                                .filter( (user: User) => user.firstName.toLowerCase().match(searchText.toLowerCase()) ||
                                                         user.lastName.toLowerCase().match(searchText.toLowerCase()) ||
                                                         user.address.toLowerCase().match(searchText.toLowerCase()) ||
                                                         user.phone.match(searchText));
        return originalArr;
    }
}
