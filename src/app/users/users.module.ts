import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {UsersListComponent} from './users-list/users-list.component';
import {UserItemComponent} from './users-list/user-item/user-item.component';
import {UserComponent} from './user/user.component';

import {UserService} from './user.service';
import {UsersPipeFilter, UsersPipeSearch} from './users.pipe';

@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        RouterModule
    ],
    declarations: [
        UsersListComponent,
        UserItemComponent,
        UserComponent,
        UsersPipeFilter,
        UsersPipeSearch
    ],
    providers: [
        UserService
    ]
})
export class UsersModule {}
