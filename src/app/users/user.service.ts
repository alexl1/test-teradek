import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {User} from '../Models/User';

@Injectable()
export class UserService {
    users: [User];

    constructor(
        protected http: HttpClient
    ) {}

    getUsers(): Observable<any> {
        return this.http.get('../../assets/users.json');
    }

    getUser(id: string) {
        if ( !this.users ) return null;

        return this.users.find( user => user.id === parseInt(id, 10));
    }
}
