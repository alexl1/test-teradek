import {Component, OnInit} from '@angular/core';

import {UserService} from '../user.service';

import {User} from '../../Models/User';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
    users: [User];
    filterType = 'order';
    searchText = '';

    constructor(
        protected userService: UserService
    ) {}

    ngOnInit() {
        this.userService.getUsers().subscribe( res => {
            this.users = res;
            this.userService.users = res;
        });
    }
}
