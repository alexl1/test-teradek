import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from '../user.service';
import {User} from '../../Models/User';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    user: User;

    constructor(
        protected userService: UserService,
        protected router: Router
    ) {}

    ngOnInit() {
        const _id = this.router.url.split('/').pop();

        this.user = this.userService.getUser(_id);

        if ( !this.user ) return this.router.navigate(['/users']);
    }

}
