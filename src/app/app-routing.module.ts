import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {UsersListComponent} from './users/users-list/users-list.component';
import {UserComponent} from './users/user/user.component';

const routes: Routes = [
    {
        path: 'users',
        component: UsersListComponent
    },
    {
        path: 'users/:id',
        component: UserComponent
    },
    {
        path: '**',
        redirectTo: '/users'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
